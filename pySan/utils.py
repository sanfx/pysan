import logging
import time
import os

from collections import namedtuple

_ntuple_diskusage = namedtuple('usage', 'total used free')

def disk_usage(path=None):
    """Return disk usage statistics about the given path.

        Returned valus is a named tuple with attributes 'total', 'used' and
        'free', which are the amount of total, used and free space, in bytes.
    """
    # if path is not passed then get full disk stats
    path = path or "/"
    st = os.statvfs(path)
    free = st.f_bavail * st.f_frsize
    total = st.f_blocks * st.f_frsize
    used = (st.f_blocks - st.f_bfree) * st.f_frsize
    return _ntuple_diskusage(total, used, free)


def sleepForAWhile(time=None, delay=0.5):
    """ Adds a delay in seconds to suspend execution
        of the calling API.
    """
    t = time or time.time()
    while (time.time() - t) < 5:
        time.sleep(delay)   
    return 1

def sizeof(bytes): 
    """ Takes the size of file or folder in bytes and 
        returns size formatted in kb, MB, GB, TB or PB. 
    """
    alternative = [ 
        (1024 ** 5, ' PB'), 
        (1024 ** 4, ' TB'), 
        (1024 ** 3, ' GB'), 
        (1024 ** 2, ' MB'), 
        (1024 ** 1, ' KB'), 
        (1024 ** 0, (' byte', ' bytes')), 
    ] 
 
    for factor, suffix in alternative: 
        if bytes >= factor: 
            break
    amount = int(bytes/factor) 
    if isinstance(suffix, tuple): 
        singular, multiple = suffix 
        if amount == 1: 
            suffix = singular 
        else: 
            suffix = multiple 
    return "%s %s" % (str(amount), suffix)


def xor(lst1, lst2):
    """ returns a tuple of items of item not in either of lists
    """
    longer, shorter = (lst2, lst1) if len(lst2) > len(lst1) else (lst1, lst2)
    return tuple(item for item in longer if item not in shorter)


def function_logger(appName, logFile, fileDebugLevel, file_level, console_level=None):

    logger = logging.getLogger(appName)
    # By default, logs all messages
    logger.setLevel(logging.DEBUG)

    if console_level:
        # StreamHandler logs to console
        ch = logging.StreamHandler()
        ch.setLevel(fileDebugLevel)
        ch_format = logging.Formatter('%(levelname)s - %(message)s')
        ch.setFormatter(ch_format)
        logger.addHandler(ch)

    fh = logging.FileHandler(logFile)
    fh.setLevel(file_level)
    fh_format = logging.Formatter('%(asctime)s - (%(threadName)-10s) - %(levelname)s:  %(message)s')
    fh.setFormatter(fh_format)
    logger.addHandler(fh)

    return logger
