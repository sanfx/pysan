import re

class InvalidTorrentException(Exception):

	def __init__(self, exception):
		super(InvalidTorrentException, self).__init__(exception)
		self._exception = exception

	def __repr__(self):
		return repr(self._exception)

def tokenize(text, match=re.compile("([idel])|(\d+):|(-?\d+)").match):
	i = 0
	while i < len(text):
		m = match(text, i)
		s = m.group(m.lastindex)
		i = m.end()
		if m.lastindex == 2:
			yield "s"
			yield text[i:i+int(s)]
			i = i + int(s)
		else:
			yield s

def decode_item(next, token):
	if token == "i":
		# integer: "i" value "e"
		data = int(next())
		if next() != "e":
			raise ValueError
	elif token == "s":
		# string: "s" value (virtual tokens)
		data = next()
	elif token == "l" or token == "d":
		# container: "l" (or "d") values "e"
		data = []
		tok = next()
		while tok != "e":
			data.append(decode_item(next, tok))
			tok = next()
		if token == "d":
			data = dict(zip(data[0::2], data[1::2]))
	else:
		raise ValueError
	return data

def decode(text):
	try:
		src = tokenize(text)
		data = decode_item(src.next, src.next())
		for token in src: # look for more tokens
			raise InvalidTorrentException("trailing junk")
	except (AttributeError, ValueError, StopIteration) as err:
		raise InvalidTorrentException(err)
	return data