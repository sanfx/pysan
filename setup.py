from distutils.core import setup
from os import path

v = open(path.join(path.dirname(__file__), 'VERSION'))
VERSION = v.readline().strip()
v.close()

setup(
    name='pySan',
    version=VERSION,
    author='Sanjeev Kumar',
    author_email='skysan@gmail.com',
    packages=['pySan',],
    data_files = ['VERSION'],
    url='https://sanfx@bitbucket.org/sanfx/pysan.git',
    license='LICENSE',
    description='Package is a collection of usefull and commonly used utility functions for all my projects development',
)
    # long_description=open('README.md').read(),
